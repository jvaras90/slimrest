<?php

if (!defined("SPECIALCONSTANT")) {
    die("Acceso Denegado");
}

//var_dump($app);


$app->get("/books/", function() use($app) {
    try {
        $connection = getConnection();
        $dbh = $connection->prepare("select * from books");
        $dbh->execute();
        $books = $dbh->fetchAll();
       // var_dump($books); 
        $connection = null;
        $app->response->headers->set("Content-type", "application/json");
        $app->response->status(200);
        $app->response->body(json_encode($books));
    } catch (PDOException $e) {
        echo "Error: " . $e->getMessage();
    }
});

$app->get("/books/:id", function($id) use($app) {
    try {
        $connection = getConnection();
        $dbh = $connection->prepare("select * from books where id = ? ");
        $dbh->bindParam(1, $id);
        $dbh->execute();
        $books = $dbh->fetch();
        $connection = null;
        $app->response->headers->set("Content-type", "application/json");
        $app->response->status(200);
        $app->response->body(json_encode($books));
    } catch (PDOException $e) {
        echo "Error: " . $e->getMessage();
    }
});

$app->post("/books/", function() use($app) {
    $tittle = $app->request->post("tittle");
    $isbn = $app->request->post("isbn");
    $author = $app->request->post("author");

    try {
        $connection = getConnection();
        $dbh = $connection->prepare("insert into books values(null,?, ?, ?, now())");
        $dbh->bindParam(1, $tittle);
        $dbh->bindParam(2, $isbn);
        $dbh->bindParam(3, $author);
        $dbh->execute();
        $bookId = $connection->lastInsertId();
        $connection = null;
        $app->response->headers->set("Content-type", "application/json");
        $app->response->status(200);
        $app->response->body(json_encode($bookId));
    } catch (PDOException $e) {
        echo "Error: " . $e->getMessage();
    }
});

$app->put("/books/", function() use($app) {
    $tittle = $app->request->post("tittle");
    $isbn = $app->request->post("isbn");
    $author = $app->request->post("author");
    $id = $app->reques->put("id");
    try {
        $connection = getConnection();
        $dbh = $connection->prepare("update books set tittle=? , isbn = ?, author = ? , create_at = now() where id = ? ");
        $dbh->bindParam(1, $tittle);
        $dbh->bindParam(2, $isbn);
        $dbh->bindParam(3, $author);
        $dbh->bindParam(4, $id);
        $dbh->execute();
        $connection = null;
        $app->response->headers->set("Content-type", "application/json");
        $app->response->status(200);
        $app->response->body(json_encode(array("res" => 1)));
    } catch (PDOException $e) {
        echo "Error: " . $e->getMessage();
    }
});

$app->delete("/books/:id", function($id) use($app) {
    try {
        $connection = getConnection();
        $dbh = $connection->prepare("delete from books where id = ?");
        $dbh->bindParam(4, $id);
        $dbh->execute();
        $connection = null;
        $app->response->headers->set("Content-type", "application/json");
        $app->response->status(200);
        $app->response->body(json_encode(array("res" => 1)));
    } catch (PDOException $e) {
        echo "Error: " . $e->getMessage();
    }
});
