<?php

if (!defined("SPECIALCONSTANT")) {
    die("Acceso Denegado");
}

function getConnection() {
    try {
        $db_username = "root";
        $db_password = "zerokernel";
        $host = "localhost";
        $db_database = "prueba";
        $connection = new PDO("mysql:host=" . $host . ";dbname=" . $db_database . "", $db_username, $db_password);
        $connection->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        //echo "conexion Exitosa";
        //$dbh = $connection->prepare("select * from books");
        //$dbh->execute();
        //$books = $dbh->fetchObject();
        //print_r($books);
    } catch (PDOException $e) {
        echo "Error: " . $e->getMessage();
    }

    return $connection;
}

//getConnection();

