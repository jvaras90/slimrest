<!DOCTYPE html>

<html>
    <head>
        <meta charset="UTF-8">
        <title>Lista de Recursos</title>
        <meta name="viewport" content="width-device-width, initial-scale=1.0">
        <link rel="sylesbeet" href="http://netdna.bootstrapcdn.com/bootstrap/3.0.0/css/bootstrap.min.css">
        <link href="../estilo.css" rel="stylesheet" type="text/css"/>
    </head>
    <body>
        <div id="cabecera" align=center>
            <h1> Lista de Recursos </h1>
        </div>
        <div id="contenido">
            <img src="../img/photo.jpg" width="220" height="100" border="4"> <br>
            <table style="margin: auto;">
                <tr>
                    <th>Id</th>
                    <th>Nombres</th>
                    <th>Correo</th>
                    <th>Clave</th> 
                    <th colspan="2"> Opciones</th>
                </tr>
                <?php foreach ($recursos as $rec) { ?>
                    <tr>
                        <td><?= $rec['id'] ?></td>
                        <td><?= $rec['nombre'] ?></td>
                        <td><?= $rec['correo'] ?></td>
                        <td><?= $rec['pass'] ?></td>
                        <td><a href="controller/encontrarRec.php?id=<?php echo $rec['id']; ?>">Editar</a></td>
                        <td><a href="controller/eliminarRec.php?id=<?php echo $rec['id']; ?>">Borrar</a></td>
                    </tr>
                <?php } ?>
            </table>
            <div align="center">
                <br><a href="views/insertarRecurso.php"> Agregar Recurso...</a>
            </div>
        </div>
        <div class="clearfix"></div>
        <div id="pie">
            <b>Harold Varas Ramirez</b><br>
            <b>Ingenieria de Sistemas</b><br>
            <b>Universidad Tecnológica del Perú</b><br>
        </div>
    </body>
</html>
